﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.eviivo.TextMatch.Core.Utils;

namespace com.eviivo.TextMatch.Core.Implementations
{
    public class Match : IMatch
    {
        private IList<int> _hits;

        public Match(IList<int> hits)
        {
            _hits = hits;
        }

        public IList<int> GetResult(string text, string subtext)
        {
            _hits.Clear();
            Matches(_hits, text, subtext);
            return _hits;
        }

        public override string ToString()
        {
            const string NO_OUTPUT = "There is no output";
            return _hits.Any() ? string.Join<int>(",", _hits) : NO_OUTPUT;
        }

        private void Matches(IList<int> hits, string text, string subtext)
        {
            if (!ValidateMatch(text, subtext))
                return;

            char firstCharMatch = subtext.First();
            int subtextLength = subtext.Length;
            var tasks = new List<Task>();
            for (int i = 0; i <= text.Length - subtextLength; i++)
            {
                int position = i;
                //If the first character is ok, check the rest of the subtext string
                if (text[position].CompareInvariants(firstCharMatch))
                #if DEBUG
                    //it's a nightmare debugging with threads
                    Check(text, subtext, position, subtextLength, hits);
                #else
                    tasks.Add(Task.Factory.StartNew(() => 
                    Check(text, subtext, position, subtextLength, hits)
                        ));
                #endif
            }
            Task.WaitAll(tasks.ToArray());
        }

        private bool ValidateMatch(string text, string subtext)
        {
            bool isValid = true;

            if (subtext.Length < 1)
                isValid = false;

            if (text.Length < subtext.Length)
                isValid = false;

            return isValid;
        }

        private void Check(string text, string subtext, int position, int subtextLength, IList<int> hits)
        {
            for (int i = 1; i < subtextLength; i++)
                if (!subtext[i].CompareInvariants(text[i + position]))
                    return;
            //if all the characters of the subtext are found, the hit is saved
            hits.Add(position);
        }

    }
}
