﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eviivo.TextMatch.Core.Utils
{
    public static class CharExtensions
    {
        public static bool CompareInvariants(this char c, char compare)
        {
            return char.ToUpperInvariant(c) == char.ToUpperInvariant(compare);
        }
    }
}
