﻿using System;
using System.Collections.Generic;
using com.eviivo.TextMatch.Core;
using com.eviivo.TextMatch.Core.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace com.eviivo.TextMatch.Test.Core
{
    [TestClass]
    public class MatchTest
    {

        private IMatch Match;

        [TestInitialize()]
        public void Initialize()
        {
            Match = new Match(new List<int>());
        }

        [TestMethod]
        public void CaseInvariantTest()
        {
            string text = "Input text is Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea";
            string subtext = "Polly";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 3, "Incorrect number of matches");
        }

        [TestMethod]
        public void EndingWordTest()
        {
            string text = "Polly";
            string subtext = "Polly";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 1, "Incorrect number of matches");
        }

        [TestMethod]
        public void NotPartOfTheTextTest()
        {
            string text = "There's no letter >< in here";
            string subtext = "X";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 0, "There's no X, so the count is wrong");
        }

        [TestMethod]
        public void MixWordsMatchTest()
        {
            string text = "PolPolPolly";
            string subtext = "Polly";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 1, "Incorrect number of matches");
        }

        [TestMethod]
        public void MixWordsMatch2Test()
        {
            string text = "lLlLl";
            string subtext = "ll";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 4, "Incorrect number of matches");
        }

        [TestMethod]
        public void BlankSpacesTest()
        {
            string text = "P\nolly po lly";
            string subtext = "polly";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 0, "Incorrect number of matches");
        }

        [TestMethod]
        public void NoInputTest()
        {
            string text = "";
            string subtext = "";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 0, "Incorrect number of matches");
        }

        [TestMethod]
        public void SubtextBiggerThanTextTest()
        {
            string text = "Polly";
            string subtext = "Pollyardo";

            var result = Match.GetResult(text, subtext);
            Assert.IsTrue(result.Count == 0, @"It shouldn't be matches since the text 
            to match is bigger than the text itself");
        }

    }
}
