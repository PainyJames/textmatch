﻿using System;
using System.Collections.Generic;
using com.eviivo.TextMatch.Steps;

namespace com.eviivo.TextMatch
{
    public class TextMatch
    {
        public static void Main(string[] args)
        {
            var initText = string.Empty;
            var initSubtext = string.Empty;
            if (args.Length > 0)
                initText = args[0] as string;
            if (args.Length > 1)
                initSubtext = args[1] as string;
            //Start
            IStep start = new Step()
            {
                Description = "TextMatch application start point",
                Title = "Main Menu",
                Text = initText,
                Subtext = initSubtext,
                Options = new List<IStep>()
            };
            //ChangeText
            IStep changeText = new ChangeTextStep()
            {
                Description = "Change text to search against",
                Title = "Change text",
                Back = start
            };
            //ChangeSubtext
            IStep changeSubtext = new ChangeSubtextStep()
            {
                Description = "Change text to search for",
                Title = "Change subtext",
                Back = start
            };
            //Search
            IStep search = new SearchTextStep()
            {
                Description = "Show the occurrences of the search",
                Title = "Search",
                Back = start
            };
            start.Options.Add(changeText);
            start.Options.Add(changeSubtext);
            start.Options.Add(search);
            start.Execute();
            
        }
    }
}
