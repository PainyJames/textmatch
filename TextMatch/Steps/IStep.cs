﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eviivo.TextMatch.Steps
{
    public interface IStep
    {
        string Text
        {
            set;
            get;
        }

        string Subtext
        {
            set;
            get;
        }

        IStep Back
        {
            set;
            get;
        }

        IList<IStep> Options
        {
            set;
            get;
        }

        string Title
        {
            set;
            get;
        }

        string Description
        {
            set;
            get;
        }

        void Execute();
    }
}
