﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eviivo.TextMatch.Steps
{
    public class Step : IStep
    {

        public virtual void Execute()
        {
            DisplayMenu();
            SelectAndExecuteOption();
        }

        protected void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine(Title + "\n");
            if (Options != null)
                for (int i = 0; i < Options.Count; i++)
                    Console.WriteLine(String.Format("{0}.- {1} - {2}", i + 1, Options[i].Title, Options[i].Description));

            if (Back != null)
                Console.WriteLine(String.Format("Esc - {0} - {1}", Back.Title, Back.Description));
            else
                Console.WriteLine("Esc - Exit");
            Console.WriteLine("\nSelect an option, please...\n\n");
            DisplayCurrentValues();

        }

        protected void DisplayCurrentValues()
        {
            Console.WriteLine(string.Format(@"Current Values:
Text: {0}
Subtext: {1}", _text, _subtext));
        }

        protected void SelectAndExecuteOption()
        {
            while (!Console.KeyAvailable)
            {
                ExecuteOption(Console.ReadKey(true));
            }
        }

        protected void ExecuteOption(ConsoleKeyInfo key)
        {

            if (key.Key == ConsoleKey.Escape)
            {
                if (Back == null)
                    Environment.Exit(0);
                else
                    Back.Execute();
            }
            else
            {
                int keyNumber = 0;
                if (Int32.TryParse((key.KeyChar - '0').ToString(), out keyNumber) && Options != null && keyNumber > 0 && keyNumber <= Options.Count)
                    Options[keyNumber - 1].Execute();
                else
                    SelectAndExecuteOption();
            }

        }

        private static string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private static string _subtext;
        public string Subtext
        {
            get { return _subtext; }
            set { _subtext = value; }
        }

        public IStep Back
        {
            get;
            set;
        }

        public IList<IStep> Options
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
