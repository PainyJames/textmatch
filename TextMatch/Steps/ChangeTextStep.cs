﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eviivo.TextMatch.Steps
{
    class ChangeTextStep : Step
    {
        public override void Execute()
        {
            Console.Clear();
            Console.WriteLine("\nEnter the text to search against\n\n");
            Text = Console.ReadLine();
            if (Back != null)
                Back.Execute();
            else
                Environment.Exit(0);
        }
    }
}
