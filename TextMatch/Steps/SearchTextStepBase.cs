﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.eviivo.TextMatch.Core;

namespace com.eviivo.TextMatch.Steps
{
    abstract class SearchTextStepBase : Step
    {
        protected IMatch Match
        {
            get;
            set;
        }

        public override void Execute()
        {
            Console.Clear();
            Console.WriteLine("\nResults of the search:");
            Match.GetResult(Text, Subtext);
            Console.WriteLine(Match.ToString());
            Console.WriteLine("\n\nPress Esc to go back");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            if (Back != null)
                Back.Execute();
            else
                Environment.Exit(0);
        }
    }
}
