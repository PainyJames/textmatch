﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.eviivo.TextMatch.Core;
using com.eviivo.TextMatch.Core.Implementations;

namespace com.eviivo.TextMatch.Steps
{
    class SearchTextStep : SearchTextStepBase
    {
        public SearchTextStep()
        {
            Match = new Match(new List<int>());
        }
    }
}
